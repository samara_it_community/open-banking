package features;

import enums.FeatureBenefitType;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "features_benefit_item")
public class FeatureBenefitItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 35)
    private String identification;

    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private FeatureBenefitType type;

    @Column(length = 350)
    private String name;

    @Column
    private String amount;

    @Column
    private boolean indicator;

    @Column(length = 500)
    private String textual;

    @ElementCollection
    @CollectionTable(name = "features_benefit_item_notes",
            joinColumns = @JoinColumn(name = "features_benefit_item_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "features_benefit_item_other_type",
            joinColumns = @JoinColumn(name = "features_benefit_item_id"))
    private OtherCodeType otherType;

    @OneToMany(mappedBy = "features_benefit_item_id")
    private List<FeatureBenefitEligibility> featureBenefitEligibility;

    @ManyToOne
    @JoinColumn(name = "features_benefit_group_id")
    private FeatureBenefitGroup featureBenefitGroup;
}
