package features;

import ccc.CccMarketingState;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "features_and_benefits")
public class FeaturesAndBenefits {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "features_and_benefits_id")
    private List<FeatureBenefitGroup> featureBenefitGroup;

    @OneToMany(mappedBy = "features_and_benefits_id")
    private List<FeatureBenefitItem> featureBenefitItem;

    @OneToOne
    @JoinColumn(name = "ccc_marketing_state_id")
    private CccMarketingState cccMarketingState;
}
