package repayment;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "repayment_allocation")
public class RepaymentAllocation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ElementCollection
    @CollectionTable(name = "repayment_allocation_notes",
            joinColumns = @JoinColumn(name = "repayment_allocation_id"))
    private List<String> notes;

    @OneToOne
    @JoinColumn(name = "repayment_id")
    private Repayment repayment;
}
