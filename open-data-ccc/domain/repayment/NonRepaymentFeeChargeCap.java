package repayment;

import enums.FeeType;
import enums.MinMaxType;
import enums.Period;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "non_repayment_fee_charge_cap")
public class NonRepaymentFeeChargeCap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ElementCollection
    @CollectionTable(name = "non_repayment_fee_charge_cap_fee_type", joinColumns = @JoinColumn(name = "non_repayment_fee_charge_cap_id"))
    @Enumerated(EnumType.STRING)
    @Column(length = 4, nullable = false)
    @Size(min = 1)
    private List<FeeType> feeType;

    @Enumerated(EnumType.STRING)
    @Column(name = "min_max_type", length = 4, nullable = false)
    private MinMaxType minMaxType;

    @Column(name = "fee_cap_occurrence")
    private int feeCapOccurrence;

    @Column(name = "fee_cap_amount")
    private String feeCapAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "capping_period")
    private Period cappingPeriod;

    @ElementCollection
    @CollectionTable(name = "non_repayment_fee_charge_cap_notes",
            joinColumns = @JoinColumn(name = "non_repayment_fee_charge_cap_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "other_fee_type", joinColumns = @JoinColumn(name = "non_repayment_fee_charge_cap_id"))
    private List<OtherCodeType> otherFeeType;

    @ManyToOne
    @JoinColumn(name = "non_repayment_fee_charges_id")
    private NonRepaymentFeeCharges nonRepaymentFeeCharges;
}
