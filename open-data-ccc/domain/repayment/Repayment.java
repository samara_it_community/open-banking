package repayment;

import ccc.CccMarketingState;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "repayment")
public class Repayment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "min_balance_repayment_rate")
    private String minBalanceRepaymentRate;

    @Column(name = "min_balance_repayment_amount")
    private String minBalanceRepaymentAmount;

    @ElementCollection
    @CollectionTable(name = "repayment_notes",
            joinColumns = @JoinColumn(name = "repayment_id"))
    private List<String> notes;

    @OneToMany(mappedBy = "repayment_id")
    private List<NonRepaymentFeeCharges> nonRepaymentFeeCharges;

    @OneToOne
    @Column(name = "repayment_allocation", nullable = false)
    private RepaymentAllocation repaymentAllocation;

    @OneToOne
    @JoinColumn(name = "ccc_marketing_state_id")
    private CccMarketingState cccMarketingState;
}
