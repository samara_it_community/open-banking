package eligibility;

import enums.MinMaxType;
import enums.Period;
import enums.TradingEligibilityType;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "trading_history_eligibility")
public class TradingHistoryEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "trading_type", length = 4)
    private TradingEligibilityType tradingType;

    @Enumerated(EnumType.STRING)
    @Column(name = "min_max_type", length = 4)
    private MinMaxType minMaxType;

    @Column
    private String amount;

    @Column
    private boolean indicator;

    @Column
    private String textual;

    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private Period period;

    @ElementCollection
    @CollectionTable(name = "trading_history_eligibility_notes",
            joinColumns = @JoinColumn(name = "trading_history_eligibility_id"))
    private List<String> notes;

    @ManyToOne
    @JoinColumn(name = "eligibility_id")
    private OtherCodeType otherTradingType;
}
