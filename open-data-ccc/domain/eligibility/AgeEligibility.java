package eligibility;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "age_eligibility")
public class AgeEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "minimum_age")
    private int minimumAge;

    @Column(name = "maximum_age")
    private int maximumAge;

    @ElementCollection
    @CollectionTable(name = "age_eligibility_notes",
            joinColumns = @JoinColumn(name = "age_eligibility_id"))
    private List<String> notes;

    @OneToOne
    @JoinColumn(name = "eligibility_id")
    private Eligibility eligibility;
}
