package eligibility;

import ccc.CccMarketingState;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "eligibility")
public class Eligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "eligibility_id")
    private List<OtherEligibility> otherEligibility;

    @OneToOne
    @JoinColumn(name = "age_eligibility_id")
    private AgeEligibility ageEligibility;

    @OneToMany(mappedBy = "eligibility_id")
    private List<ResidencyEligibility> residencyEligibility;

    @OneToMany(mappedBy = "eligibility_id")
    private List<TradingHistoryEligibility> tradingHistoryEligibility;

    @OneToMany(mappedBy = "eligibility_id")
    private List<LegalStructureEligibility> legalStructureEligibility;

    @OneToMany(mappedBy = "eligibility_id")
    private List<OfficerEligibility> officerEligibility;

    @OneToOne
    @JoinColumn(name = "id_Eligibility_id")
    private IdEligibility idEligibility;

    @OneToOne
    @JoinColumn(name = "credit_check_eligibility_id")
    private CreditCheckEligibility creditCheckEligibility;

    @OneToOne
    @JoinColumn(name = "industry_eligibility_id")
    private IndustryEligibility industryEligibility;

    @OneToOne
    @JoinColumn(name = "ccc_marketing_state_id")
    private CccMarketingState cccMarketingState;
}
