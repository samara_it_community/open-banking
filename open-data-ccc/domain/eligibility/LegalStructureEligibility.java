package eligibility;

import enums.LegalStructure;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "legal_structure_eligibility")
public class LegalStructureEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "legal_structure", length = 4)
    private LegalStructure legalStructure;

    @ElementCollection
    @CollectionTable(name = "legal_structure_eligibility_notes",
            joinColumns = @JoinColumn(name = "legal_structure_eligibility_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "legal_structure_eligibility_types",
            joinColumns = @JoinColumn(name = "legal_structure_eligibility_id"))
    private OtherCodeType otherLegalStructure;

    @ManyToOne
    @JoinColumn(name = "eligibility_id")
    private OtherCodeType otherTradingType;
}
