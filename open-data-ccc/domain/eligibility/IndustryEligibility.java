package eligibility;

import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "industry_eligibility")
public class IndustryEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ElementCollection
    @CollectionTable(name = "sic_code_included",
            joinColumns = @JoinColumn(name = "industry_eligibility_id"))
    private List<String> sicCodeIncluded;

    @ElementCollection
    @CollectionTable(name = "sic_code_excluded",
            joinColumns = @JoinColumn(name = "industry_eligibility_id"))
    private List<String> sicCodeExcluded;

    @ElementCollection
    @CollectionTable(name = "industry_eligibility_notes",
            joinColumns = @JoinColumn(name = "industry_eligibility_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "other_sic_code_included", joinColumns = @JoinColumn(name = "industry_eligibility_id"))
    private List<OtherCodeType> otherSicCodeIncluded;

    @ElementCollection
    @CollectionTable(name = "other_sic_code_excluded", joinColumns = @JoinColumn(name = "industry_eligibility_id"))
    private List<OtherCodeType> otherSicCodeExcluded;

    @OneToOne
    @JoinColumn(name = "eligibility_id")
    private Eligibility eligibility;
}
