package eligibility;

import enums.OfficerType;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "officer_eligibility")
public class OfficerEligibility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "officer_type", length = 4)
    private OfficerType officerType;

    @Column(name = "max_amount")
    private int maxAmount;

    @Column(name = "min_amount")
    private int minAmount;

    @ElementCollection
    @CollectionTable(name = "officer_eligibility_notes",
            joinColumns = @JoinColumn(name = "officer_eligibility_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "other_officer_type_types",
            joinColumns = @JoinColumn(name = "other_officer_type_id"))
    private OtherCodeType otherOfficerType;

    @ManyToOne
    @JoinColumn(name = "eligibility_id")
    private OtherCodeType otherTradingType;
}
