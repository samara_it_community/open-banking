package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum TradingEligibilityType {
    TEAR("AnnualReturns",
            "AnnualReturns required to be eligible for the account"),
    TECJ("PreviousCCJSAllowed",
            "States whether previous CCJs are eligible for the account"),
    TEGH("GoodTradingHistory",
            "States that only those companies with a good trading history are eligible"),
    TEOT("Other",
            "Provide other trading eligibility"),
    TEBP("PreviousBankruptcyAllowed",
            "States whether previous bankruptcies (or involuntary agreements (IVAs)) are eligible for the account"),
    TETL("TradingLength",
            "TradingLength required to be eligible for the account"),
    TETR("Turnover",
            "Turnover required to be eligible for the account");

    public final String codeName;
    public final String description;

    TradingEligibilityType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static TradingEligibilityType fromName(String codeName) {
        for (TradingEligibilityType tradingEligibilityType : TradingEligibilityType.values()) {
            if (tradingEligibilityType.codeName.equals(codeName)) {
                return tradingEligibilityType;
            }
        }
        throw new IllegalArgumentException("No such TradingEligibilityType.");
    }
}
