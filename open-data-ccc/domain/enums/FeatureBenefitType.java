package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum FeatureBenefitType {
    FBBT("AccountManagement", "Covers any feature/benefit of managing the account that " +
            "the bank wishes to highlight e.g. Text Alerts"),
    FBCB("CashBack", "This credit card is a cashback credit card which pays the card product " +
            "holder a certain amount depending on the value of purchases made with the card."),
    FBCR("CreditReports", "Covers free credit reports"),
    FBLS("Lifestyle", "Account offers lifestyle-related benefits e.g. concierge services, " +
            "access to airport lounge"),
    FBMB("MotorBreakdown", "Account offers reduced prices for motor breakdown e.g. RAC"),
    FBOT("Other", "Fill out the OtherFeatureBenefitType fields for any " +
            "Feature/Benefit types not covered"),
    FBPH("PaymentHolidays", "Card offers Payment holidays where no payment is made on a " +
            "repayment scheduled date"),
    FBSL("SpendLimits", "Spending limits can be set against individual cards and/or the " +
            "associated credit card account"),
    FBTI("TravelInsurance", "Card offers reduced and enhanced travel insurance");

    public final String codeName;
    public final String description;

    FeatureBenefitType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static FeatureBenefitType fromName(String codeName) {
        for (FeatureBenefitType featureBenefitType : FeatureBenefitType.values()) {
            if (featureBenefitType.codeName.equals(codeName)) {
                return featureBenefitType;
            }
        }
        throw new IllegalArgumentException("No such FeatureBenefitType.");
    }
}
