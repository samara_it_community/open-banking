package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum MarketingState {
    CAPR("Promotional",
            "Marketing state where certain characteristics of the product differ from the regular" +
                    " offering e.g. lower  rates or higher interest rates for an initial period."),
    CARE("Regular",
            "The features form the regular offering of the product");

    public final String codeName;
    public final String description;

    MarketingState(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static MarketingState fromName(String codeName) {
        for (MarketingState marketingState : MarketingState.values()) {
            if (marketingState.codeName.equals(codeName)) {
                return marketingState;
            }
        }
        throw new IllegalArgumentException("No such MarketingState.");
    }
}
