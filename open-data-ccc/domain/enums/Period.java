package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum Period {
    PDAY("Day", "Day"),
    PHYR("Half Year", "Half Year"),
    PMTH("Month", "Month"),
    PQTR("Quarter", "Quarter"),
    PWEK("Week", "Week"),
    PYER("Year", "Year");

    public final String codeName;
    public final String description;

    Period(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static Period fromName(String codeName) {
        for (Period period : Period.values()) {
            if (period.codeName.equals(codeName)) {
                return period;
            }
        }
        throw new IllegalArgumentException("No such Period.");
    }
}
