package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum ResidencyType {
    RTIN("Incorporated", "Where the company was incorporated"),
    RTOR("Owner", "Where the owner is resident"),
    RTOT("Other", "Other residency types"),
    RTTR("Trading", "Where the company is resident for trading purposes"),
    RTTX("CompanyTax", "Where the company is resident for tax purposes");

    public final String codeName;
    public final String description;

    ResidencyType(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static ResidencyType fromName(String codeName) {
        for (ResidencyType residencyType : ResidencyType.values()) {
            if (residencyType.codeName.equals(codeName)) {
                return residencyType;
            }
        }
        throw new IllegalArgumentException("No such ResidencyType.");
    }
}
