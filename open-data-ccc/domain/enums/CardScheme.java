package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum CardScheme {
    CSMC("Mastercard", "Mastercard"),
    CSOT("Other", "Other card scheme"),
    CSVI("Visa", "Visa");

    public final String codeName;
    public final String description;

    CardScheme(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static CardScheme fromName(String codeName) {
        for (CardScheme cardScheme : CardScheme.values()) {
            if (cardScheme.codeName.equals(codeName)) {
                return cardScheme;
            }
        }
        throw new IllegalArgumentException("No such CardScheme.");
    }
}
