package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum Frequency {
    FQDY("Daily",
            "Calculated and/or applied daily"),
    FQHY("HalfYearly",
            "Calculated and/or applied half yearly"),
    FQMY("Monthly",
            "Calculated and/or applied monthly"),
    FQOT("Other",
            "Other Calculation and/or application frequency"),
    FQQY("Quarterly",
            "Calculated and/or applied weekly"),
    FQSD("PerStatementDate",
            "Calculated and/or applied on the statement date"),
    FQWY("Weekly",
            "Calculated and/or applied weekly"),
    FQYY("Yearly",
            "Calculated and/or applied annually");

    public final String codeName;
    public final String description;

    Frequency(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static Frequency fromName(String codeName) {
        for (Frequency frequency : Frequency.values()) {
            if (frequency.codeName.equals(codeName)) {
                return frequency;
            }
        }
        throw new IllegalArgumentException("No such Frequency.");
    }
}
