package enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat
public enum FeeFrequency {
    SMDA("Daily", "The payment interval is daily"),
    SMFL("Flexible", "The payment interval is flexible"),
    SMFO("Fortnightly", "The payment interval is fortnightly"),
    SMHO("Holiday", "The payment interval is based on a period where no payments are made (payment holiday)"),
    SMHY("HalfYearly", "The payment interval is half yearly"),
    SMMO("Monthly", "The payment interval is monthly"),
    SMOT("Other", "Any other fee frequency types"),
    SMQU("Quarterly", "The payment interval is quarterly"),
    SMWE("Weekly", "The payment interval is weekly"),
    SMYE("Yearly", "The payment interval is yearly");

    public final String codeName;
    public final String description;

    FeeFrequency(String codeName, String description) {
        this.codeName = codeName;
        this.description = description;
    }

    @JsonValue
    public String getName() {
        return codeName;
    }

    @JsonCreator
    public static FeeFrequency fromName(String codeName) {
        for (FeeFrequency feeFrequency : FeeFrequency.values()) {
            if (feeFrequency.codeName.equals(codeName)) {
                return feeFrequency;
            }
        }
        throw new IllegalArgumentException("No such FeeFrequency. Tried codeName - " + codeName);
    }
}
