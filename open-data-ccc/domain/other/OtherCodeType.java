package other;

import lombok.Data;

import javax.persistence.*;

@Data
@Embeddable
public class OtherCodeType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "code", length = 4)
    private String code;

    @Column(name = "name", length = 70)
    private String name;

    @Column(name = "description", length = 350)
    private String description;
}
