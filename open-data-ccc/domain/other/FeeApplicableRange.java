package other;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "fee_applicable_range")
public class FeeApplicableRange {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "minimum_amount")
    private String minimumAmount;

    @Column(name = "maximum_amount")
    private String maximumAmount;

    @Column(name = "minimum_rate")
    private String minimumRate;

    @Column(name = "maximum_rate")
    private String maximumRate;

    @OneToOne
    @JoinColumn(name = "fee_charge_detail")
    private FeeChargeDetail feeChargeDetail;
}
