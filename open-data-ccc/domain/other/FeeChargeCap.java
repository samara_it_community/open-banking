package other;

import enums.FeeType;
import enums.MinMaxType;
import enums.Period;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "fee_charge_cap")
public class FeeChargeCap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ElementCollection
    @CollectionTable(name = "fee_charge_cap_fee_type", joinColumns = @JoinColumn(name = "fee_charge_cap_id"))
    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private List<FeeType> feeType;

    @Enumerated(EnumType.STRING)
    @Column(name = "min_max_type", length = 4)
    private MinMaxType minMaxType;

    @Column(name = "fee_cup_occurrence")
    private int feeCupOccurrence;

    @Column(name = "fee_cap_amount")
    private String feeCapAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "capping_period", length = 4)
    private Period cappingPeriod;

    @ElementCollection
    @CollectionTable(name = "fee_charge_cap_notes",
            joinColumns = @JoinColumn(name = "fee_charge_cap_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "fee_charge_cap__other_fee_type",
            joinColumns = @JoinColumn(name = "fee_charge_cap__id"))
    private OtherCodeType otherFeeType;

    @ManyToOne
    @JoinColumn(name = "other_fee_charges_id")
    private OtherFeeCharges otherFeeCharges;
}
