package ccc;

import enums.ProductSegment;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Table(name="ccc")
public class Ccc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 350, nullable = false)
    private String name;

    @Column(length = 40, nullable = false)
    private String identification;

    @ElementCollection
    @CollectionTable(name = "ccc_segments", joinColumns = @JoinColumn(name = "ccc_id"))
    @Enumerated(EnumType.STRING)
    @Column(length = 4, nullable = false)
    @Size(min = 1)
    private List<ProductSegment> segment;

    @ElementCollection
    @CollectionTable(name = "ccc_other_segments", joinColumns = @JoinColumn(name = "ccc_id"))
    private List<OtherCodeType> otherSegment;

    @OneToMany(mappedBy = "ccc_id")
    @Size(min = 1)
    private List<CccMarketingState> cccMarketingState;
}
