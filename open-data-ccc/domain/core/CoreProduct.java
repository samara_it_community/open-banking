package core;

import ccc.CccMarketingState;
import enums.CardScheme;
import enums.Period;
import enums.SalesAccessChannels;
import enums.ServicingAccessChannels;
import lombok.Data;
import other.OtherCodeType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "core_product")
public class CoreProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "product_url", length = 500, nullable = false)
    private String productUrl;

    @Column(name = "product_url", length = 2000)
    private String productDescription;

    @Column(name = "product_url", length = 500)
    private String tscAndCsUrl;

    @Column(name = "max_daily_card_withdrawal_limit")
    private String maxDailyCardWithdrawalLimit;

    @Column(name = "min_credit_limit")
    private String minCreditLimit;

    @Column(name = "max_credit_limit")
    private String maxCreditLimit;

    @Column(name = "max_purchase_interest_free_length_days")
    private int maxPurchaseInterestFreeLengthDays;

    @ElementCollection
    @CollectionTable(name = "sales_access_channels",
            joinColumns = @JoinColumn(name = "core_product_id"))
    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private List<SalesAccessChannels> salesAccessChannels;

    @ElementCollection
    @CollectionTable(name = "servicing_access_channels",
            joinColumns = @JoinColumn(name = "core_product_id"))
    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private List<ServicingAccessChannels> servicingAccessChannels;

    @ElementCollection
    @CollectionTable(name = "card_scheme",
            joinColumns = @JoinColumn(name = "core_product_id"))
    @Enumerated(EnumType.STRING)
    @Column(length = 4)
    private List<CardScheme> cardScheme;

    @Column(name = "contactless_indicator")
    private boolean contactlessIndicator;

    @Column(name = "periodic_fee")
    private String periodicFee;

    @Enumerated(EnumType.STRING)
    @Column(name = "periodic_fee_period")
    private Period periodicFeePeriod;

    @Column
    private String apr;

    @ElementCollection
    @CollectionTable(name = "core_product_notes",
            joinColumns = @JoinColumn(name = "core_product_id"))
    private List<String> notes;

    @ElementCollection
    @CollectionTable(name = "core_product_other_card_scheme", joinColumns = @JoinColumn(name = "core_product_id"))
    private List<OtherCodeType> otherCardScheme;

    @OneToOne
    @JoinColumn(name = "ccc_marketing_state_id")
    private CccMarketingState cccMarketingState;
}
