package domain;

import lombok.Data;
import lombok.NonNull;

@Data
public class Amount {
    @NonNull
    private double amount;
    @NonNull
    private String currency;
}
