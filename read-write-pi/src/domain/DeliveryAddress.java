package domain;

import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
public class DeliveryAddress {
    private List<String> addressLine;
    private String streetName;
    private String buildingNumber;
    private String postCode;
    @NonNull
    private String townName;
    private List<String> countrySubDivision;
    @NonNull
    private String country;
}
