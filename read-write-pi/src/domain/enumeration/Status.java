package domain.enumeration;

public enum Status {
    AUTHORISED("Authorised"),
    AWAITINGFURTHERAUTHORISATION("AwaitingFurtherAuthorisation"),
    REJECTED("Rejected");

    private String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
