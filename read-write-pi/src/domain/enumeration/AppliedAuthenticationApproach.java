package domain.enumeration;

public enum AppliedAuthenticationApproach {
    SCA("SCA"),
    CA("CA");

    private String value;

    AppliedAuthenticationApproach(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
