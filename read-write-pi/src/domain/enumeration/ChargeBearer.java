package domain.enumeration;

public enum ChargeBearer {
    BORNEBYCREDITOR("BorneByCreditor"),
    BORNEBYDEBTOR("BorneByDebtor"),
    FOLLOWINGSERVICELEVEL("FollowingServiceLevel"),
    OTHER("Other"),
    SHARED("Shared");

    private String value;

    ChargeBearer(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}

