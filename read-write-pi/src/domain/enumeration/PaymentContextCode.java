package domain.enumeration;

public enum PaymentContextCode {
    BILLPAYMENT("BillPayment"),
    ECOMMERCEGOODS("EcommerceGoods"),
    ECOMMERCESERVICES("EcommerceServices"),
    OTHER("Other"),
    PARTYTOPARTY("PartyToParty");

    private String value;

    PaymentContextCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}

