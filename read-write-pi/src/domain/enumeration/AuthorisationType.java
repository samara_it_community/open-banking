package domain.enumeration;

public enum  AuthorisationType {
    ANY("Any"),
    SINGLE("Single");

    private String value;

    AuthorisationType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
