package domain;

import domain.enumeration.TransactionIndividualExtendedISOStatus;
import lombok.Data;
import lombok.NonNull;

import java.util.Date;

@Data
public class OBWritePaymentDetails1 {
    @NonNull
    private String paymentTransactionId;
    @NonNull
    private TransactionIndividualExtendedISOStatus status;
    @NonNull
    private Date statusUpdateDateTime;
    private StatusDetail statusDetail;
}
