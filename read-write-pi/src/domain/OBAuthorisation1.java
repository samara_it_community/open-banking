package domain;

import domain.enumeration.AuthorisationType;
import lombok.Data;
import lombok.NonNull;

import java.util.Date;

@Data
public class OBAuthorisation1 {
    @NonNull
    private AuthorisationType authorisationType;
    private Date completionDateTime;
}
