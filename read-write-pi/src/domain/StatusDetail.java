package domain;

import domain.enumeration.StatusReason;
import lombok.Data;
import lombok.NonNull;

@Data
public class StatusDetail {
    private String localInstrument;
    @NonNull
    private String status;
    private StatusReason statusReason;
    private String statusReasonDescription;
}
