package domain;

import domain.enumeration.ChargeBearer;
import lombok.Data;
import lombok.NonNull;

@Data
public class OBCharge2 {
    @NonNull
    private ChargeBearer chargeBearer;
    @NonNull
    private String type;
    @NonNull
    private Amount amount;
}
