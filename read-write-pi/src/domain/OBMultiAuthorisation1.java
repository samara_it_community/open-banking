package domain;

import domain.enumeration.Status;
import lombok.Data;
import lombok.NonNull;

import java.util.Date;

@Data
public class OBMultiAuthorisation1 {
    @NonNull
    private Status status;
    private Integer numberRequired;
    private Integer numberReceived;
    private Date lastUpdateDateTime;
    private Date expirationDateTime;
}
