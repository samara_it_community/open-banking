import enums.ATMServices;
import enums.Accessibility;
import lombok.Data;
import java.util.Set;

@Data
public class ATM {
    private String Identification;
    private Set<String> SupportedLanguages;
    private Set<ATMServices> ATMServices;
    private Set<Accessibility> Accessibility;
    private boolean Access24HoursIndicator;
    private Set<String> SupportedCurrencies;
    private String MinimumPossibleAmount;
    private String Note;
    private Set<OtherCodeType> otherAccessibility;
    private Set<OtherCodeType> otherATMServices;
    private Branch branch;
    private Location location;
}
