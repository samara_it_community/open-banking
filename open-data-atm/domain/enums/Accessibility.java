package enums;

public enum Accessibility {
    ATAC("AudioCashMachine"),
    ATAD("AutomaticDoors"),
    ATER("ExternalRamp"),
    ATIL("InductionLoop"),
    ATIR("InternalRamp"),
    ATLA("LevelAccess"),
    ATLL("LowerLevelCounter"),
    ATOT("Other"),
    ATWA("WheelchairAccess");

    private String value;

    Accessibility(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

}
