import lombok.Data;

@Data
public class GeoLocation {
    private GeographicalCoordinates geographicalCoordinates;
}
