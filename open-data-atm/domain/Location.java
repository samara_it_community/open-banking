import lombok.Data;

import java.util.Set;

@Data
public class Location {
    private Site site;
    private PostalAddress postalAddress;
    private Set<OtherCodeType> otherLocationCategory;
    private Set<String> LocationCategory;
}
