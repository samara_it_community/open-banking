package domain;

import java.util.List;
import java.util.Set;

import domain.codes.SMELProductSegmentCode;
import lombok.Data;

@Data
public class SMELoan {

    private String name;
    private String identification;
    private Set<SMELProductSegmentCode> segment;
    private Set<OtherCodeType> otherSegment;
    private List<SMELoanMarketingState> sMELoanMarketingState;
	
}