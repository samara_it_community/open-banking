package domain;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class IndustryEligibility {

    private Set<String> sICCodeIncluded;
    private Set<String> sICCodeExcluded;
    private List<String> notes;
    private List<OtherSICCodeType> otherSICCodeIncluded;
    private List<OtherSICCodeType> otherSICCodeExcluded;

}
