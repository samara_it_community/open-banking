package domain;

import domain.codes.LoanApplicationFeeChargedTypeCode;
import domain.codes.SalesAccessChannelsCode;
import domain.codes.ServicingAccessChannelsCode;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class CoreProduct {

    private String productDescription;
    private String productURL;
    private String tcsAndCsURL;
    private Set<SalesAccessChannelsCode> salesAccessChannels;
    private Set<ServicingAccessChannelsCode> servicingAccessChannels;
    private boolean earlyPaymentFeeApplicable;
    private boolean overPaymentFeeApplicable;
    private LoanApplicationFeeChargedTypeCode loanApplicationFeeChargeType;
    private boolean overpaymentAllowedIndicator;
    private boolean fullEarlyRepaymentAllowedIndicator;
    private List<String> notes;

}
