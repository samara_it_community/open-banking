package domain;

import domain.codes.MarketingStateCode;
import domain.codes.PeriodCode;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class SMELoanMarketingState {

    private String identification;
    private String predecessorID;
    private MarketingStateCode marketingState;
    private LocalDate firstMarketedDate;
    private LocalDate lastMarketedDate;
    private int stateTenureLength;
    private PeriodCode stateTenurePeriod;
    private List<String> notes;
    private LoanInterest loanInterest;
    private List<Repayment> repayment;
    private Eligibility eligibility;
    private FeaturesAndBenefits featuresAndBenefits;
    private OtherFeesAndCharges otherFeesCharges;
    private CoreProduct coreProduct;

}
