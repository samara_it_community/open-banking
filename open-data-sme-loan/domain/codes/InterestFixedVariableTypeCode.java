package domain.codes;

public enum InterestFixedVariableTypeCode {
    INFI("Fixed"),
    INVA("Variable");

    private String value;

    InterestFixedVariableTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
