package domain.codes;

public enum RepaymentFrequencyCode {
    SMDA("Daily"),
    SMFL("Flexible"),
    SMFO("Fortnightly"),
    SMHY("HalfYearly"),
    SMMO("Monthly"),
    SMOT("Other"),
    SMQU("Quarterly"),
    SMWE("Weekly"),
    SMYE("Yearly");

    private String value;

    RepaymentFrequencyCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
