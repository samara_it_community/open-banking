package domain.codes;

public enum InterestRateTypeCode {
    INBB("BOEBaseRate"),
    INFR("FixedRate"),
    INGR("Gross"),
    INLR("LoanProviderBaseRate"),
    INNE("Net"),
    INOT("Other");

    private String value;

    InterestRateTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
