package domain.codes;

public enum FeeCategoryCode {
    FCBD("BankersDrafts"),
    FCCS("CounterServices"),
    FCFN("Foreign"),
    FCLG("Legal"),
    FCOL("Online"),
    FCOT("Other"),
    FCPS("PaymentScheme"),
    FCPY("Penalty"),
    FCRE("Repayment"),
    FCRP("Report"),
    FCSK("Safekeeping"),
    FCSV("Servicing"),
    FCTR("Transaction");

    private String value;

    FeeCategoryCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
