package domain.codes;

import java.util.List;

public enum SMELProductSegmentCode {
	GEAS("AgricultureSector"),
	GEBU("Business"),
	GEFB("FlexibleBusinessLoan"),
	GEFG("FixedGroup"),
	GEGS("GovernmentScheme"),
	GEOT("Other"),
	GESS("SectorSpecific");
	
	private String value;
	
	SMELProductSegmentCode(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}