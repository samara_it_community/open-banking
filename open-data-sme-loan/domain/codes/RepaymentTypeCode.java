package domain.codes;

public enum RepaymentTypeCode {
    USBA("Balloon"),
    USBU("Bullet"),
    USCI("CapitalAndInterest"),
    USCS("CustomSchedule"),
    USER("EarlyRepayment"),
    USFA("FixedCapitalFullyAmortising"),
    USFB("FixedCapitalWithBullet"),
    USFI("FixedCapitalAndInterestReducingBalance"),
    USIO("InterestOnly"),
    USOT("Other"),
    USPF("PrepaymentFee"),
    USRW("RepaymentWithBullet"),
    USSL("StraightLineInterestOnly");

    private String value;

    RepaymentTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
