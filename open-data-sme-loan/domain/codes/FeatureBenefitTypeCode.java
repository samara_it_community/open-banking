package domain.codes;

public enum FeatureBenefitTypeCode {
    FBES("EarlySettlement"),
    FBFR("FlexibleRepayment"),
    FBOP("OverPayment"),
    FBOT("Other"),
    FBPH("PaymentHolidays"),
    FBRH("RepaymentHoliday");

    private String value;

    FeatureBenefitTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
