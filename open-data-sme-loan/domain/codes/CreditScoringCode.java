package domain.codes;

public enum CreditScoringCode {
    CAHA("Hard"),
    CASF("Soft");

    private String value;

    CreditScoringCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
