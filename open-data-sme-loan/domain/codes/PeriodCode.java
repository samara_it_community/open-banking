package domain.codes;

public enum PeriodCode {
    PDAY("Day"),
    PHYR("Half Year"),
    PMTH("Month"),
    PQTR("Quarter"),
    PWEK("Week"),
    PYER("Year");

    private String value;

    PeriodCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
