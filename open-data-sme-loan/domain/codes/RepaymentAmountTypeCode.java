package domain.codes;

public enum RepaymentAmountTypeCode {
    RABD("BalanceToDate"),
    RABL("Balloon"),
    RACI("CapitalAndInterest"),
    RAFC("FeeChargeCap"),
    RAIO("InterestOnly"),
    RALT("Bullet"),
    USOT("Other");

    private String value;

    RepaymentAmountTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
