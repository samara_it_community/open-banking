package domain.codes;

public enum SMELEligibilityTypeCode {
    ELBO("BusinessOnly"),
    ELCS("CreditScoring"),
    ELEC("ExistingCustomers"),
    ELIAV("IdAndV"),
    ELMO("Mortgage"),
    ELNA("NoArrearsOnLoan"),
    ELNC("NoCustomerInArrears"),
    ELNO("NewCustomersOnly"),
    ELOT("Other"),
    ELST("StartUp"),
    ELTR("Turnover");

    private String value;

    SMELEligibilityTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
