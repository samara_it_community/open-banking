package domain.codes;

public enum InterestCalculationMethodCode {
    ITCO("Compound"),
    ITSI("SimpleInterest");

    private String value;

    InterestCalculationMethodCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
