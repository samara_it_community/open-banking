package domain.codes;

public enum TradingEligibilityTypeCode {
    TEAR("AnnualReturns"),
    TECJ("PreviousCCJSAllowed"),
    TEGH("GoodTradingHistory"),
    TEOT("Other"),
    TEPB("PreviousBankruptcyAllowed"),
    TETL("TradingLength"),
    TETR("Turnover");

    private String value;

    TradingEligibilityTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
