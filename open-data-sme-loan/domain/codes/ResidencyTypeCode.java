package domain.codes;

public enum ResidencyTypeCode {
    RTIN("Incorporated"),
    RTOR("Owner"),
    RTOT("Other"),
    RTTR("Trading"),
    RTTX("CompanyTax");

    private String value;

    ResidencyTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
