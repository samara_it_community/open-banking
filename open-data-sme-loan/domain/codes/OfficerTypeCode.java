package domain.codes;

public enum OfficerTypeCode {
    OFDR("Director"),
    OFOR("Owner"),
    OFOT("Other"),
    OFPR("Partner"),
    OFSC("SignificantControl");

    private String value;

    OfficerTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
