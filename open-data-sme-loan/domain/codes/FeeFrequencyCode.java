package domain.codes;

public enum FeeFrequencyCode {
    FEAC("OnClosing"),
    FEAO("OnOpening"),
    FECP("ChargingPeriod"),
    FEDA("Daily"),
    FEHO("Holiday"),
    FEI("PerItem"),
    FEMO("Monthly"),
    FEOA("OnAnniversary"),
    FEOT("Other"),
    FEPC("PerHundredPounds"),
    FEPH("PerHour"),
    FEPO("PerOccurrence"),
    FEPS("PerSheet"),
    FEPT("PerTransaction"),
    FEPTA("PerTransactionAmount"),
    FEPTP("PerTransactionPercentage"),
    FEQU("Quarterly"),
    FESM("SixMonthly"),
    FEST("StatementMonthly"),
    FEWE("Weekly"),
    FEYE("Yearly");

    private String value;

    FeeFrequencyCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
