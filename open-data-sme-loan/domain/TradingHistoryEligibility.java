package domain;

import domain.codes.MinMaxTypeCode;
import domain.codes.PeriodCode;
import domain.codes.TradingEligibilityTypeCode;
import lombok.Data;

import java.util.List;

@Data
public class TradingHistoryEligibility {

    private TradingEligibilityTypeCode tradingType;
    private MinMaxTypeCode minMaxType;
    private String amount;
    private boolean indicator;
    private String textual;
    private PeriodCode period;
    private List<String> notes;
    private OtherCodeType otherTradingType;

}
