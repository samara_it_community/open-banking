package domain;

import domain.codes.CreditScoringCode;
import lombok.Data;

import java.util.List;

@Data
public class CreditCheckEligibility {

    private CreditScoringCode scoringType;
    private List<String> notes;

}
