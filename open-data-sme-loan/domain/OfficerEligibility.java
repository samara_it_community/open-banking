package domain;

import domain.codes.OfficerTypeCode;
import lombok.Data;

import java.util.List;

@Data
public class OfficerEligibility {

    private OfficerTypeCode officerType;
    private int minAmount;
    private int maxAmount;
    private List<String> notes;
    private OtherCodeType otherOfficerType;

}
