package domain;

import lombok.Data;

import java.util.List;

@Data
public class OtherFeesAndCharges {

    private List<FeeChargeDetail> feeChargeDetail;
    private List<FeeChargeCap> feeChargeCap;

}
