package domain;

import lombok.Data;

import java.util.List;

@Data
public class LoanInterestFeesCharge {

    private List<LoanInterestFeeChargeDetail> loanInterestFeeChargeDetail;
    private List<LoanInterestFeeChargeCap> loanInterestFeeChargeCap;

}
