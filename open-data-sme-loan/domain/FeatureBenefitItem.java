package domain;

import domain.codes.FeatureBenefitTypeCode;
import lombok.Data;

import java.util.List;

@Data
public class FeatureBenefitItem {

    private String identification;
    private FeatureBenefitTypeCode type;
    private String name;
    private String amount;
    private boolean indicator;
    private String textual;
    private List<String> notes;
    private OtherCodeType otherType;
    private List<FeatureBenefitEligibility> featureBenefitEligibility;

}
