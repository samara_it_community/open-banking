package domain;

import domain.codes.LegalStructureCode;
import lombok.Data;

import java.util.List;

@Data
public class LegalStructureEligibility {

    private LegalStructureCode legalStructure;
    private List<String> notes;
    private OtherCodeType otherLegalStructure;

}
