package domain;

import lombok.Data;

@Data
public class OtherSICCodeType {

    private String code;
    private String name;
    private String description;

}
