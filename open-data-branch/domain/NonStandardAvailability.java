package domain;

import java.time.LocalDate;
import java.util.List;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "non_standard_availabilities")
public class NonStandardAvailability {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToMany
	@Size(max = 7)
	private List<Day> day;

	@Column(length = 140, nullable = false)
	private String name;

	@Column
	private LocalDate startDate;

	@Column
	private LocalDate endDate;

	@Column(length = 2000)
	private String notes;
}
