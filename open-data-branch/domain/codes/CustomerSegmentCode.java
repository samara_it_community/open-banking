package domain.codes;

public enum CustomerSegmentCode {
	BRBU("Business"),
	BRCO("Corporate"),
	BROT("Other"),
	BRPE("Personal"),
	BRPI("Private"),
	BRPR("Premier"),
	BRSE("Select"),
	BRSM("SME"),
	BRWE("Wealth");

	private String value;

	CustomerSegmentCode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
}
