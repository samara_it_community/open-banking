package domain.codes;

public enum AccessibilityCode {
	BRAD("AutomaticDoors"),
	BRCM("AudioCashMachine"),
	BRER("ExternalRamp"),
	BRHH("HelpingHandUnit"),
	BRIL("InductionLoop"),
	BRIR("InternalRamp"),
	BRLA("LevelAccess"),
	BRLL("LowerLevelCounter"),
	BROT("Other"),
	BRWA("WheelChairAccess");

	private String value;

	AccessibilityCode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
}
