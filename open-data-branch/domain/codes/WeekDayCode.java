package domain.codes;

public enum WeekDayCode {
	SUND("Sunday"),
	MOND("Monday"),
	THUD("Thursday"),
	WEDD("Wednesday"),
	TUED("Tuesday"),
	FRID("Friday"),
	SATD("Saturday");

	private String value;

	WeekDayCode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
}
