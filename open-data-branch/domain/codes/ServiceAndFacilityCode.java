package domain.codes;

public enum ServiceAndFacilityCode {
	BRAS("AssistedServiceCounter"),
	BRAT("ExternalATM"),
	BRAV("AccountVerificationService"),
	BRBC("BusinessCounter"),
	BRBD("BureauDeChange"),
	BRBP("BusinessDepositTerminal"),
	BRBS("BusinessITSupport"),
	BRCI("CardIssuanceFacility"),
	BRCL("CollectionLockers"),
	BRCS("CounterServices"),
	BREQ("ExternalQuickServicePoint"),
	BRIQ("InternalQuickServicePoint"),
	BRIT("InternalATM"),
	BRLD("LodgementDevice"),
	BRMA("MortgageAdvisor"),
	BRMR("MeetingRooms"),
	BRNS("NightSafe"),
	BROB("OnlineBankingPoint"),
	BROD("OnDemandCurrency"),
	BROT("Other"),
	BRPA("Parking"),
	BRPC("PremierCounter"),
	BRQD("QuickDeposit"),
	BRSC("SaturdayCounterService"),
	BRSP("StatementPrinter"),
	BRSS("SelfServiceAccountOpening"),
	BRVB("VideoBanking"),
	BRWI("WiFi");

	private String value;

	ServiceAndFacilityCode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return value;
	}
}
