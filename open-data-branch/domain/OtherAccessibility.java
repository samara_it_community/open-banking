package domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "other_accessibilities")
public class OtherAccessibility {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String code;

	@Column(length = 70, nullable = false)
	private String name;

	@Column(length = 350, nullable = false)
	private String description;
}
