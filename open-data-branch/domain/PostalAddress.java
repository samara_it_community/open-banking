package domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name = "postal_addresses")
public class PostalAddress {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ElementCollection
	@CollectionTable(name = "address_lines",
			joinColumns = @JoinColumn(name = "postal_address_id"))
	@Size(max = 7)
	@Column(length = 70)
	private List<String> addressLine;

	@Column(length = 350)
	private String buildingNumber;

	@Column(length = 70)
	private String streetName;

	@Column(length = 35)
	private String townName;

	@ElementCollection
	@CollectionTable(name = "country_sub_divisions",
			joinColumns = @JoinColumn(name = "postal_address_id"))
	@Size(max = 2)
	@Column(length = 35)
	private List<String> countrySubDivision;

	@Column(length = 2)
	private String country;

	@Column(length = 16, nullable = false)
	private String postCode;

	@OneToOne
	private GeoLocation geoLocation;
}
